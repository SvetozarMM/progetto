<img  src="src/assets/logo_dark.png" alt="landing" width="300px" style="margin-top: 20px;"/>


# Progetto - Project Management System by Team 3


Progetto is an end-to-end platform aimed at managing projects, project teams and team leaders. Fully implemented functionalities for creating, time tracking and managing the ongoing or upcoming projects of a company. 
The system  allows for administrators and/or managers to submit new entries for employees, competences, projects, positions while minding important data such workday lengths, announcements and so on.


## Project Overview

### How to run it:
  1. First clone th project from the repository
  2. Run ``` npm install``` in order to install all needed dependencies
  3. Open a terminal in the root directory and run  ``` ng serve --open ```
  4. Use the following account:
 
      username: test@testing.com <br>
      password: Aaa123
      
### Technologies used:
  - Angular 9
  - Firestore

### The Wellcome screen visible to the public
<img src="src/assets/log_form.JPG" alt="landing" width="300px" style="margin-top: 20px;"/>


### The Dashboard after login
<img src="src/assets/dash.JPG" alt="landing" width="700px" style="margin-top: 20px;"/>


### The Pojects Section
<img src="src/assets/projects.JPG" alt="landing" width="700px" style="margin-top: 20px;"/>

### And the create project view
<img src="src/assets/create_project.JPG" alt="landing" width="700px" style="margin-top: 20px;"/>


#### And that's only a teaser for the unique experience with our system.
<br>


### The Team

The project was created by **Svetozar Martinov** and **Georgi Priporski**.



