export enum StatusType {
    Active = 'Active',
    Planning = 'Planning',
    Ended = 'Ended',
    Completed = 'Completed',
}
