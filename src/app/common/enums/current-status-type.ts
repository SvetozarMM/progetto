export enum CurrentStatusType {
    WithinTarget = 'Within Target',
    OverdueTarget = 'Overdue Target',
    Pending = 'Pending',
}
