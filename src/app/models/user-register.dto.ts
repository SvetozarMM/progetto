import { SkillDTO } from './skill.dto';
import { UserRegisterProjectDTO } from './user-register-projects.dto';
import { EmployeeSubordinatetDTO } from './employee-subordinate.dto';
import { UserDirectManagerDTO } from './user-direct-manager.dto';

export interface RegUser {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  position: string;
  freeTime: number;
  avatarURL?: string;
  directManager?: UserDirectManagerDTO;
  selfManaged?: boolean;
  isManager?: boolean;
  isAdmin?: boolean;
  uid: string;
  skills?: string[];
  currentProjects?: UserRegisterProjectDTO[];
  subordinates?: EmployeeSubordinatetDTO[];
}
