export interface EmployeeProjectDTO {
    id: string;
    firstName: string;
    lastName: string;
    currentWorkHours: number;
}
