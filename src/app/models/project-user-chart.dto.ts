export interface ProjectUserChartDTO {
    id?: string;
    name: string;
    value: number;
}
