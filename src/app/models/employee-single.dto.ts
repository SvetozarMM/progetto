import { SkillDTO } from './skill.dto';
import { UserRegisterProjectDTO } from './user-register-projects.dto';
import { UserDirectManagerDTO } from './user-direct-manager.dto';

export interface SingleEmployee {
  id: string;
  firstName: string;
  lastName: string;
  position: string;
  freeTime?: number;
  avatarURL?: string;
  directManager?: UserDirectManagerDTO;
  isManager?: boolean;
  isAdmin?: boolean;
  skills?: string[];
  currentProjects?: UserRegisterProjectDTO[];
}
