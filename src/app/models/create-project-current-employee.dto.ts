export interface CreateProjectCurrentEmployeeDTO {
    id: string;
    firstName: string;
    lastName: string;
    freeTime: number;
    skills: string[];
    currentProjects: string[];
    currentFreeTime: number;
    currentWorkHours: number;
}
