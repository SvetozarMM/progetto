export interface Roles {
  isAdmin: boolean;
  isManager: boolean;
}
