export interface UserRegisterProjectDTO {
  id: string;
  name: string;
  description: string;
}
