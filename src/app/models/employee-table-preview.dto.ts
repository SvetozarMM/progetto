import { UserDirectManagerDTO } from './user-direct-manager.dto';
import { SkillDTO } from './skill.dto';

export interface EmployeeTablePreviewDTO {
  id: string;
  firstName: string;
  lastName: string;
  position: string;
  freeTime: number;
  directManagerFn: UserDirectManagerDTO;
  directManagerLn: UserDirectManagerDTO;
  currentProjects: string[];
  skills: SkillDTO[];
}
