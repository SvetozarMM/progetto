import { UserDirectManagerDTO } from './user-direct-manager.dto';
import { EmployeeSubordinatetDTO } from './employee-subordinate.dto';

export interface UserTablePreviewDTO {
  uid: string;
  firstName: string;
  lastName: string;
  position: string;
  freeTime: number;
  selfManaged?: boolean;
  directManagerFn: UserDirectManagerDTO;
  directManagerLn: UserDirectManagerDTO;
  subordinates?: EmployeeSubordinatetDTO[];
}
