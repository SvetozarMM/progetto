import { ProjectReporterDTO } from './project-reporter.dto';
import { SkillBoxDTO } from './skill-box.dto';

export interface ProjectDTO {
    id: string;
    name: string;
    description: string;
    target: number;
    complete: string;
    start: Date;
    end: Date;
    reporter: ProjectReporterDTO;
    status: string;
    currentStatus: string;
    managementTime: number;
    projectUrl: string;
    skillsBoxList: SkillBoxDTO[];
}
