import { Roles } from './roles.model';

export interface User {
  email?: string;
  userId?: string;
  // roles: Roles;
  firstName?: string;
  lastName?: string;
}
