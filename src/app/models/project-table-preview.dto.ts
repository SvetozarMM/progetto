export interface ProjectTablePreviewDTO {
    id: string;
    name: string;
    reporterId: string;
    reporterFirstName: string;
    reporterLastName: string;
    start: string;
    end: string;
    status: string;
    target: number;
    managementTime: number;
    currentStatus: string;
    complete: string;
    projectUrl: string;
}
