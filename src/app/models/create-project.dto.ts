import { ProjectReporterDTO } from './project-reporter.dto';
import { SkillBoxDTO } from './skill-box.dto';

export interface CreateProjectDTO {
    name: string;
    description: string;
    target: number;
    complete: string;
    start: string;
    end: string;
    reporter: ProjectReporterDTO;
    status: string;
    currentStatus: string;
    managementTime: number;
    skillsBoxList: SkillBoxDTO[];
    projectUrl: string;
}
