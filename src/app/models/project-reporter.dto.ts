export interface ProjectReporterDTO {
    id: string;
    firstName: string;
    lastName: string;
}
