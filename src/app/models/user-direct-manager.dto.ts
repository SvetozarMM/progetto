export interface UserDirectManagerDTO {
    id: string;
    firstName: string;
    lastName: string;
    avatarURL: string;
}
