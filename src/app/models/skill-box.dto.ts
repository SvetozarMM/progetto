import { SkillDTO } from './skill.dto';
import { EmployeeProjectDTO } from './employee-project.dto';

export interface SkillBoxDTO {
    skill: SkillDTO;
    target: number;
    complete: string;
    employeesList: EmployeeProjectDTO[];
}
