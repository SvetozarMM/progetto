import { UserDirectManagerDTO } from './user-direct-manager.dto';
import { UserRegisterProjectDTO } from './user-register-projects.dto';

export interface SingleUser {
  uid: string;
  firstName: string;
  lastName: string;
  position: string;
  freeTime?: number;
  avatarURL?: string;
  directManager?: UserDirectManagerDTO;
  isManager?: boolean;
  isAdmin?: boolean;
  currentProjects?: UserRegisterProjectDTO[];
}
