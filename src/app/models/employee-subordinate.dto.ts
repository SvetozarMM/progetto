export interface EmployeeSubordinatetDTO {
  id: string;
  firstName: string;
  lastName: string;
  avatarURL: string;
}
