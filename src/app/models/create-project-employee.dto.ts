export interface CreateProjectEmployeeDTO {
    id: string;
    firstName: string;
    lastName: string;
    freeTime: number;
    skills: string[];
    currentProjects: string[];
}
