import { SkillBoxDTO } from './skill-box.dto';

export interface UpdateProjectDTO {
    id: string;
    name: string;
    description: string;
    target: number;
    complete: string;
    end: string;
    status: string;
    currentStatus: string;
    managementTime: number;
    skillsBoxList: SkillBoxDTO[];
    projectUrl: string;
}
