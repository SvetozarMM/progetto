export interface ProjectDashboardDTO {
    id: string;
    name: string;
    description: string;
    start: string;
    end: string;
    status: string;
    target: number;
    complete: string;
    managementTime: number;
    currentStatus: string;
    projectUrl: string;
}
