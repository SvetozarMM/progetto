import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { RegUser } from 'src/app/models/user-register.dto';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  user: RegUser;

  constructor(
    private authService: AuthService,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
        }
      });
  }

  onLogout() {
    this.authService.logout();
  }
}
