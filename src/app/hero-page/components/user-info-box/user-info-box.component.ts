import { Component, OnInit } from '@angular/core';
import { RegUser } from 'src/app/models/user-register.dto';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { ProjectDashboardDTO } from 'src/app/models/project-dashboard.dto';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user-info-box',
  templateUrl: './user-info-box.component.html',
  styleUrls: ['./user-info-box.component.css']
})
export class UserInfoBoxComponent implements OnInit {

  user: RegUser;
  ownProjects: ProjectDashboardDTO[];

  constructor(
    private readonly projectService: ProjectsService,
    private readonly userService: UserService,
  ) {}

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
          this.projectService.getAllOwnProjects(this.user.uid)
            .subscribe(projectData => this.ownProjects = projectData);
        }
      });
  }

}
