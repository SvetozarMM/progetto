import { Component, OnInit } from '@angular/core';
import { RegUser } from 'src/app/models/user-register.dto';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  panelOpenState = false;
  user: RegUser;

  constructor(
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
        }
      });
  }

  onPanelOpenState() {
    this.panelOpenState = true;
  }

}
