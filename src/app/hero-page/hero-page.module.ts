import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { MaterialModule } from '../material.module';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { UserInfoBoxComponent } from './components/user-info-box/user-info-box.component';

@NgModule({
  declarations: [
    SidenavComponent,
    HeaderComponent,
    UserInfoBoxComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    SharedModule,
    NgxChartsModule,
  ],
  exports: [
    SidenavComponent,
    HeaderComponent,
  ]
})
export class HeroPageModule { }
