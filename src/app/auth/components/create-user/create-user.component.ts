import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { UserService } from '../../../core/services/user.service';

import { Router } from '@angular/router';

import { UserDirectManagerDTO } from 'src/app/models/user-direct-manager.dto';
import { AvatarDialogComponent } from '../avatar-dialog/avatar-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EmployeeSubordinatetDTO } from '../../../models/employee-subordinate.dto';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  registerForm: FormGroup;

  directManager: UserDirectManagerDTO;
  currentUsersList: UserDirectManagerDTO[];
  userList: UserDirectManagerDTO[];

  subordinatesList: EmployeeSubordinatetDTO[];
  subordinate: EmployeeSubordinatetDTO;
  currentSubList: EmployeeSubordinatetDTO[];
  tempSubList: EmployeeSubordinatetDTO[];

  avatarUrl = 'https://i0.wp.com/www.mvhsoracle.com/wp-content/uploads/2018/08/default-avatar.jpg?ssl=1';

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    public dialog: MatDialog,
    ) { }

  ngOnInit(): void {
    this.userService.getAllManagers()
    .subscribe(data => this.userList = data);

    this.userService.getAllSubordinates()
      .subscribe(data => this.subordinatesList = data);

    this.registerForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl('', {
        validators: [Validators.required]
      }),
      firstName: new FormControl('', {
        validators: [Validators.required]
      }),
      lastName: new FormControl('', {
        validators: [Validators.required]
      }),
      position: new FormControl('', {
        validators: [Validators.required]
      }),
      isManager: new FormControl(),
      isAdmin: new FormControl(),
      selfManaged: new FormControl(),
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(AvatarDialogComponent, {
      height: '400px',
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.avatarUrl = result.avatarUrl;
      }
    });
  }

  onSubmit() {
    this.authService.registerUser({
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      position: this.registerForm.value.position,
      avatarURL: this.avatarUrl,
      freeTime: 8,
      directManager: this.directManager,
      isManager: this.registerForm.value.isManager,
      isAdmin: this.registerForm.value.isAdmin,
      selfManaged: this.registerForm.value.selfManaged,
      currentProjects: [],
      subordinates: [this.subordinate],
      uid: '',
    }).subscribe((res) => {
      console.log(res);
    });
    this.router.navigate(['home/employees']);
  }

  saveSelectedBoss(directManager: UserDirectManagerDTO) {
    if (directManager) {
      this.directManager = directManager;
    }
  }

  getUsers() {
    this.currentUsersList = this.userList;
  }

  saveSelectedSubordinate(subordinate: EmployeeSubordinatetDTO) {
    if (subordinate) {
      this.subordinate = subordinate;
    }
  }

  getSubordinates() {
    this.currentSubList = this.subordinatesList;
  }





}
