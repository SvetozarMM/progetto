import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { UserService } from '../../../core/services/user.service';
import { User } from '../../../models/user.model';
import { SkillDTO } from '../../../models/skill.dto';
import { CreateProjectService } from '../../../projects-section/services/create-project.service';
import { SkillBoxDTO } from '../../../models/skill-box.dto';
import { Router } from '@angular/router';
import { UserDirectManagerDTO } from 'src/app/models/user-direct-manager.dto';
import { AvatarDialogComponent } from '../avatar-dialog/avatar-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { EmployeeService } from '../../../core/services/employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  registerForm: FormGroup;

  directManager: UserDirectManagerDTO;
  currentUsersList: UserDirectManagerDTO[];
  userList: UserDirectManagerDTO[];

  skill: SkillDTO;
  skillList: SkillDTO[];
  currentSkillsList: SkillDTO[];

  avatarUrl = 'https://i0.wp.com/www.mvhsoracle.com/wp-content/uploads/2018/08/default-avatar.jpg?ssl=1';


  constructor(
    private authService: AuthService,
    private userService: UserService,
    private employeeService: EmployeeService,
    private readonly createProjectService: CreateProjectService,
    private router: Router,
    public dialog: MatDialog,
    private fb: FormBuilder,
    ) { }

  ngOnInit(): void {
    this.employeeService.getAllSkills()
      .subscribe(data => this.skillList = data);

    this.userService.getAllManagers()
    .subscribe(data => this.userList = data);

    this.registerForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl('', {
        validators: [Validators.required]
      }),
      firstName: new FormControl('', {
        validators: [Validators.required]
      }),
      lastName: new FormControl('', {
        validators: [Validators.required]
      }),
      position: new FormControl('', {
        validators: [Validators.required]
      }),
      directManager: new FormControl(),
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(AvatarDialogComponent, {
      height: '400px',
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.avatarUrl = result.avatarUrl;
      }
    });
  }

  onSubmit() {
    this.authService.registerEmployee({
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      position: this.registerForm.value.position,
      avatarURL: this.avatarUrl,
      freeTime: 8,
      directManager: this.directManager,
      isManager: false,
      isAdmin: false,
      skills: [this.skill.name],
      currentProjects: [],
    }).subscribe((res) => {
      console.log(res);
    });
    this.router.navigate(['home/employees']);

  }

  saveSelectedBoss(directManager: UserDirectManagerDTO) {
    if (directManager) {
      this.directManager = directManager;
    }
  }

  getUsers() {
    this.currentUsersList = this.userList;
  }

  saveSelectedSkill(skill: SkillDTO) {
    if (skill) {
      this.skill = skill;
    }
  }

  getSkills() {
    this.currentSkillsList = this.skillList;
  }

}
