import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { ProjectBoxMidComponent } from './components/project-box-mid/project-box-mid.component';
import { PendingProjectsChartComponent } from './components/pending-projects-chart/pending-projects-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CalendarBoxComponent } from './components/calendar-box/calendar-box.component';
import { MonthCalendarModule } from 'simple-angular-calendar';
import { FormsModule } from '@angular/forms';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    ProjectBoxMidComponent,
    PendingProjectsChartComponent,
    CalendarBoxComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    NgxChartsModule,
    MonthCalendarModule,
  ],
  exports: [
    ProjectBoxMidComponent,
    PendingProjectsChartComponent,
    CalendarBoxComponent,
  ]
})
export class SharedModule { }
