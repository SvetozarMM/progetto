import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingProjectsChartComponent } from './pending-projects-chart.component';

describe('PendingProjectsChartComponent', () => {
  let component: PendingProjectsChartComponent;
  let fixture: ComponentFixture<PendingProjectsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingProjectsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingProjectsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
