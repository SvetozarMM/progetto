import { Component, OnInit } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { RegUser } from 'src/app/models/user-register.dto';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { UserService } from 'src/app/core/services/user.service';
import { ProjectUserChartDTO } from 'src/app/models/project-user-chart.dto';

@Component({
  selector: 'app-pending-projects-chart',
  templateUrl: './pending-projects-chart.component.html',
  styleUrls: ['./pending-projects-chart.component.css']
})
export class PendingProjectsChartComponent implements OnInit {

  user: RegUser;
  projectsList: ProjectUserChartDTO[] = [];

  // Chart options
  view = [300, 270];
  legendTitle = 'Active Projects:';
  showLegend = true;
  showLabels = true;
  isDoughnut = true;
  legendPosition = 'bottom';
  colorScheme = {
    domain: ['#10ac84', '#d4a800', '#ee5253', '#2d98da', '#d2dae2', 'pink', 'orange', 'burlywood'],
  };

  constructor(
    private readonly projectService: ProjectsService,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
          this.projectService.getAllOwnProjectsToChart(this.user.uid)
            .subscribe(projectData => {
              this.projectsList = projectData;
            });
        }
      });
  }

}
