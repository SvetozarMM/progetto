import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectBoxMidComponent } from './project-box-mid.component';

describe('ProjectBoxMidComponent', () => {
  let component: ProjectBoxMidComponent;
  let fixture: ComponentFixture<ProjectBoxMidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectBoxMidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectBoxMidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
