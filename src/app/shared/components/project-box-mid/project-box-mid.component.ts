import { Component, OnInit, Input } from '@angular/core';
import { ProjectDashboardDTO } from 'src/app/models/project-dashboard.dto';
import * as moment from 'moment';
import { Router } from '@angular/router';
const momentBusinessDays = require('moment-business-days');

@Component({
  selector: 'app-project-box-mid',
  templateUrl: './project-box-mid.component.html',
  styleUrls: ['./project-box-mid.component.css']
})
export class ProjectBoxMidComponent implements OnInit {

  @Input()
  project: ProjectDashboardDTO;
  daysLeft: number;

  constructor(
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.calculateLeftDate();
  }

  openProject(projectUrl: string) {
    this.router.navigate(['/home/projects', projectUrl]);
  }

  private calculateLeftDate(): void {
    const endDate = moment(this.project.end, 'MM-DD-YYYY').toDate();
    this.daysLeft = momentBusinessDays(new Date()).businessDiff(momentBusinessDays(endDate)) + 1;
  }
}
