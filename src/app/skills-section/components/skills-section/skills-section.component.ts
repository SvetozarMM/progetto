import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { SkillDTO } from 'src/app/models/skill.dto';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SkillsService } from 'src/app/core/services/skills.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/core/services/notification.service';


@Component({
  selector: 'app-skills-section',
  templateUrl: './skills-section.component.html',
  styleUrls: ['./skills-section.component.css']
})
export class SkillsSectionComponent implements OnInit, AfterViewInit {

  allSkills: SkillDTO[];
  //
  addSkillForm: FormGroup;
  //
  displayedColumns: string[] = ['name'];
  dataSource: MatTableDataSource<SkillDTO>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private readonly skillsService: SkillsService,
    private readonly notificator: NotificationService,

  ) { }

  ngOnInit(): void {
    this.addSkillForm = new FormGroup({
      skillName: new FormControl('', {
        validators: [
          Validators.required,
          Validators.maxLength(20),
        ]
      }),
    });
  }

  ngAfterViewInit() {
    this.skillsService
      .getAllSkills()
      .subscribe(data => {
        this.allSkills = data;
        this.dataSource = new MatTableDataSource(this.allSkills);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addSkill() {
    if (this.allSkills.find(item => item.name === this.addSkillForm.value.skillName)) {
      this.notificator.warning('This skill already exist!');
    } else {
      this.skillsService.addSkill(this.addSkillForm.value.skillName);
      this.addSkillForm.reset();
    }
  }
}
