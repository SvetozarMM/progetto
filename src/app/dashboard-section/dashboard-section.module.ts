import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { DashboardSectionComponent } from './components/dashboard-section/dashboard-section.component';
import { DashboardProjectsBoxComponent } from './components/dashboard-projects-box/dashboard-projects-box.component';
import { SharedModule } from '../shared/shared.module';
import { DashboardProfileBoxComponent } from './components/dashboard-profile-box/dashboard-profile-box.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@NgModule({
  declarations: [
    DashboardSectionComponent,
    DashboardProjectsBoxComponent,
    DashboardProfileBoxComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    NgxChartsModule,
  ],
})
export class DashboardSectionModule { }
