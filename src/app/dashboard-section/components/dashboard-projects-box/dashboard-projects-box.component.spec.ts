import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardProjectsBoxComponent } from './dashboard-projects-box.component';
import { PendingProjectsChartComponent } from 'src/app/shared/components/pending-projects-chart/pending-projects-chart.component';
import { ProjectBoxMidComponent } from 'src/app/shared/components/project-box-mid/project-box-mid.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { UserService } from 'src/app/core/services/user.service';
import { of } from 'rxjs';
import { NgxChartsModule } from '@swimlane/ngx-charts';

describe('DashboardProjectsBoxComponent', () => {
  let component: DashboardProjectsBoxComponent;
  let fixture: ComponentFixture<DashboardProjectsBoxComponent>;
  let userService;
  let mockedUser;
  let mockedProject;
  let projectService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    mockedUser  = {
      id: '123qwe',
      username: 'user',
      email: 'userEmail',
    };

    mockedProject  = {
      id: '123qwe',
      name: 'projectName',
    };

    projectService = {
      getAllUsers() {
        return of([mockedUser]);
      },
      getAllUsersForTable() {},
      findUserById() {},
      getUserOwnProjects() {},
      getAvatars() {},
    };

    userService = {
      getAllProjectsForTable() {
        return of([mockedUser]);
      },
      findProjectByUrl() {},
      getAllOwnProjects() {},
      getAllOwnProjectsToChart() {},
    };

    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        NgxChartsModule,
      ],
      declarations: [
        DashboardProjectsBoxComponent,
        PendingProjectsChartComponent,
        ProjectBoxMidComponent
      ],
      providers: [
        ProjectsService,
        UserService,
      ]
    })
    .overrideProvider(ProjectsService, { useValue: projectService })
    .overrideProvider(UserService, { useValue: userService })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(DashboardProjectsBoxComponent);
      component = fixture.componentInstance;
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardProjectsBoxComponent);
    component = fixture.componentInstance;
});

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
