import { Component, OnInit } from '@angular/core';
import { RegUser } from 'src/app/models/user-register.dto';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-dashboard-profile-box',
  templateUrl: './dashboard-profile-box.component.html',
  styleUrls: ['./dashboard-profile-box.component.css']
})
export class DashboardProfileBoxComponent implements OnInit {

  user: RegUser;
  //
  single = [];
  view = [350, 150];
  colorScheme = {
    domain: ['#05c46b', '#e55039']
  };
  label = 'Hours';

  constructor(
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
          this.getUserTime();
        }
      });
  }

  private getUserTime() {
    const single = [
      { name: 'Work Time', value: 8 - this.user.freeTime },
      { name: 'Free Time', value: this.user.freeTime }
    ];
    this.single = single;
  }

}
