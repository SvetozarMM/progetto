import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HeroPageModule } from './hero-page/hero-page.module';
import { LoginModule } from './auth/login/login.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProjectsSectionModule } from './projects-section/projects-section.module';
import { SharedModule } from './shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DashboardSectionModule } from './dashboard-section/dashboard-section.module';
import { AuthService } from './core/services/auth.service';
import { CreateUserModule } from './auth/components/create-user.module';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './core/services/user.service';
import { SkillsSectionModule } from './skills-section/skills-section.module';
import { EmploeeSectionModule } from './emploee-section/emploee-section.module';
import { EmployeeService } from './core/services/employee.service';
import { HomeComponentComponent } from './home-component.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    MaterialModule,
    HeroPageModule,
    LoginModule,
    FlexLayoutModule,
    ProjectsSectionModule,
    SharedModule,
    NgxChartsModule,
    DashboardSectionModule,
    CreateUserModule,
    HttpClientModule,
    SkillsSectionModule,
    EmploeeSectionModule,

  ],
  providers: [AuthService, UserService, EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
