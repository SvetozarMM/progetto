import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { DashboardSectionComponent } from './dashboard-section/components/dashboard-section/dashboard-section.component';
import { HomeComponentComponent } from './home-component.component';
import { CreateUserComponent } from './auth/components/create-user/create-user.component';
import { CreateEmployeeComponent } from './auth/components/create-employee/create-employee.component';
import { ProjectsSectionComponent } from './projects-section/components/projects-section/projects-section.component';
import { EmploeeSectionComponent } from './emploee-section/components/emploee-section/emploee-section.component';
import { SingleEmployeeComponent } from './emploee-section/components/single-employee/single-employee.component';
import { EmployeeResolverService } from './core/services/employee-resolver.service';
import { SingleUserComponent } from './emploee-section/components/single-user/single-user.component';
import { UserResolverService } from './core/services/user-resolver.service';
import { SingleProjectComponent } from './projects-section/components/single-project/single-project.component';
import { ProjectResolverService } from './core/services/project-resolver.service';
import { SkillsSectionComponent } from './skills-section/components/skills-section/skills-section.component';
import { UpdateProjectBoxComponent } from './projects-section/components/update-project-box/update-project-box.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';




const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'home', component: HomeComponentComponent, canActivate: [AuthGuard], children: [
      { path: 'register', component: CreateUserComponent },
      { path: 'register-emp', component: CreateEmployeeComponent },
      {
        path: 'dashboard',
        component: DashboardSectionComponent
      },
      { path: 'projects', component: ProjectsSectionComponent },
      { path: 'employees', component: EmploeeSectionComponent },
      {
        path: 'employees/:id', component: SingleEmployeeComponent, resolve: {
          employee: EmployeeResolverService
        }
      },
      {
        path: 'users/:uid', component: SingleUserComponent, resolve: {
          user: UserResolverService
        }
      },
      {
        path: 'projects/:projectUrl',
        component: SingleProjectComponent,
        resolve: {
          project: ProjectResolverService
        }
      },
      {
        path: 'projects/:projectUrl/update',
        component: UpdateProjectBoxComponent,
        resolve: {
          project: ProjectResolverService
        },
        canActivate: [AuthGuard]
      },
      { path: 'skills', component: SkillsSectionComponent },
    ],
  },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
