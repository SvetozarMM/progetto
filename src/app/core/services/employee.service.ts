import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { EmployeeTablePreviewDTO } from '../../models/employee-table-preview.dto';
import { map, first } from 'rxjs/operators';
import { SingleEmployee } from '../../models/employee-single.dto';
import { SkillDTO } from '../../models/skill.dto';

@Injectable()
export class EmployeeService {


  constructor(
    public afAuth: AngularFireAuth,
    public db: AngularFirestore,
  ) {}

  getAllEmployeesForTable(): Observable<EmployeeTablePreviewDTO[]> {
    return this.db
      .collection('employees')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.data()['id'],
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              position: doc.payload.doc.data()['position'],
              freeTime: doc.payload.doc.data()['freeTime'],
              directManagerFn: doc.payload.doc.data()['directManager'],
              directManagerLn: doc.payload.doc.data()['directManager'],
              currentProjects: doc.payload.doc.data()['currentProjects'],
              skills: doc.payload.doc.data()['skills'].name,
            };
          });
        }),
      );
  }

  findEmployeeById(id: string): Observable<SingleEmployee> {
    return this.db.collection('employees',
      ref => ref.where('id', '==', id))
      .snapshotChanges()
      .pipe(
        map(docArray => {
          const employees = docArray
          .map(doc => {
            return {
              id: doc.payload.doc.data()['id'],
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              position: doc.payload.doc.data()['position'],
              skills: doc.payload.doc.data()['skills'],
            };
          });
          return employees.length ? employees[0] : undefined;
        }),
        first()
        );
      }

  getAllSkills(): Observable<SkillDTO[]> {
    return this.db
      .collection('skills')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
            };
          });
        }),
      );
  }
}
