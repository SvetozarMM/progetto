import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToggleCreateProjectBoxService {

  private isCreateProjectBoxOpen = new BehaviorSubject(false);
  currentIsCreateProjectBoxOpen$ = this.isCreateProjectBoxOpen.asObservable();

  constructor() { }

  show() {
    this.isCreateProjectBoxOpen.next(true);
  }

  hide() {
    this.isCreateProjectBoxOpen.next(false);
  }
}
