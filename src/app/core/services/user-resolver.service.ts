import { SingleUser } from '../../models/user-single.dto';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<SingleUser>{

  constructor(
    private readonly userService: UserService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<SingleUser> {
    const uid = route.paramMap.get('uid');

    return this.userService.findUserById(uid);
  }
}
