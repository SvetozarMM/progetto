import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { switchMap, map, first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RegUser } from '../../models/user-register.dto';
import { User } from '../../models/user.model';
import { EmployeeTablePreviewDTO } from '../../models/employee-table-preview.dto';
import { UserTablePreviewDTO } from '../../models/user-table-preview.dto';
import { SingleUser } from '../../models/user-single.dto';
import { ProjectDashboardDTO } from '../../models/project-dashboard.dto';
import { UserDirectManagerDTO } from '../../models/user-direct-manager.dto';
import { EmployeeSubordinatetDTO } from '../../models/employee-subordinate.dto';

@Injectable()
export class UserService {

  user$: Observable<any>;
  userList: User[];

  constructor(
    public afAuth: AngularFireAuth,
    public db: AngularFirestore,
  ) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.db.doc(`users/${user.uid}`).valueChanges();
        } else {
          return null;
        }
      })
    );
  }

  getAllManagers(): Observable<UserDirectManagerDTO[]> {
    return this.db
      .collection('users')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              avatarURL: doc.payload.doc.data()['avatarURL'],
            };
          });
        }),
      );
  }

  getAllSubordinates(): Observable<EmployeeSubordinatetDTO[]> {
    return this.db
      .collection('employees')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              avatarURL: doc.payload.doc.data()['avatarURL'],
            };
          });
        }),
      );
  }

  getAllUsersForTable(): Observable<UserTablePreviewDTO[]> {
    return this.db
      .collection('users')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              uid: doc.payload.doc.data()['uid'],
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              position: doc.payload.doc.data()['position'],
              freeTime: doc.payload.doc.data()['freeTime'],
              directManagerFn: doc.payload.doc.data()['directManager'],
              directManagerLn: doc.payload.doc.data()['directManager'],
              subordinates: doc.payload.doc.data()['subordinates'],
            };
          });
        }),
      );
  }

  findUserById(uid: string): Observable<SingleUser> {
    return this.db.collection('users',
      ref => ref.where('uid', '==', uid))
      .snapshotChanges()
      .pipe(
        map(docArray => {
          const users = docArray
          .map(doc => {
            return {
              uid: doc.payload.doc.data()['uid'],
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              position: doc.payload.doc.data()['position'],
              skills: doc.payload.doc.data()['skills'],
            };
          });
          return users.length ? users[0] : undefined;
        }),
        first()
        );
      }

  getUserOwnProjects(reporterId: string): Observable<ProjectDashboardDTO[]> {
    return this.db
      .collection('projects',
      ref => ref.where('reporter.id', '==', reporterId)
                .where('status', '==', 'Active'))
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
              description: doc.payload.doc.data()['description'],
              start: doc.payload.doc.data()['start'],
              end: doc.payload.doc.data()['end'],
              status: doc.payload.doc.data()['status'],
              target: doc.payload.doc.data()['target'],
              complete: doc.payload.doc.data()['complete'],
              managementTime: doc.payload.doc.data()['managementTime'],
              currentStatus: doc.payload.doc.data()['currentStatus'],
              projectUrl: doc.payload.doc.data()['projectUrl'],
            };
          });
        }),
      );
  }

  getAvatars(){
    return this.db.collection('avatars').valueChanges();
}

}
