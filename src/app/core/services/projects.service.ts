import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, first } from 'rxjs/operators';
import { ProjectTablePreviewDTO } from 'src/app/models/project-table-preview.dto';
import { ProjectDTO } from '../../models/project.dto';
import { ProjectDashboardDTO } from 'src/app/models/project-dashboard.dto';
import { ProjectUserChartDTO } from 'src/app/models/project-user-chart.dto';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(
    private readonly db: AngularFirestore,
  ) { }

  getAllProjectsForTable(): Observable<ProjectTablePreviewDTO[]> {
    return this.db
      .collection('projects')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
              reporterId: doc.payload.doc.data()['reporter'].id,
              reporterFirstName: doc.payload.doc.data()['reporter'].firstName,
              reporterLastName: doc.payload.doc.data()['reporter'].lastName,
              start: doc.payload.doc.data()['start'],
              end: doc.payload.doc.data()['end'],
              status: doc.payload.doc.data()['status'],
              target: doc.payload.doc.data()['target'],
              managementTime: doc.payload.doc.data()['managementTime'],
              currentStatus: doc.payload.doc.data()['currentStatus'],
              complete: doc.payload.doc.data()['complete'],
              projectUrl: doc.payload.doc.data()['projectUrl'],
            };
          });
        }),
      );
  }

  findProjectByUrl(projectUrl: string): Observable<ProjectDTO> {
    return this.db.collection('projects',
      ref => ref.where('projectUrl', '==', projectUrl))
      .snapshotChanges()
      .pipe(
        map(docArray => {
          const projects = docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
              description: doc.payload.doc.data()['description'],
              target: doc.payload.doc.data()['target'],
              complete: doc.payload.doc.data()['complete'],
              start: doc.payload.doc.data()['start'],
              end: doc.payload.doc.data()['end'],
              reporter: doc.payload.doc.data()['reporter'],
              status: doc.payload.doc.data()['status'],
              currentStatus: doc.payload.doc.data()['currentStatus'],
              managementTime: doc.payload.doc.data()['managementTime'],
              projectUrl: doc.payload.doc.data()['projectUrl'],
              skillsBoxList: doc.payload.doc.data()['skillsBoxList'],
            };
          });
          return projects.length ? projects[0] : undefined;
        }),
        first()
      );
  }

  getAllOwnProjects(reporterId: string): Observable<ProjectDashboardDTO[]> {
    return this.db
      .collection('projects',
      ref => ref.where('reporter.id', '==', reporterId)
                .where('status', 'in', ['Active', 'Planning']))
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
              description: doc.payload.doc.data()['description'],
              start: doc.payload.doc.data()['start'],
              end: doc.payload.doc.data()['end'],
              status: doc.payload.doc.data()['status'],
              target: doc.payload.doc.data()['target'],
              complete: doc.payload.doc.data()['complete'],
              managementTime: doc.payload.doc.data()['managementTime'],
              currentStatus: doc.payload.doc.data()['currentStatus'],
              projectUrl: doc.payload.doc.data()['projectUrl'],
            };
          });
        }),
      );
  }

  getAllOwnProjectsToChart(reporterId: string): Observable<ProjectUserChartDTO[]> {
    return this.db
      .collection('projects',
      ref => ref.where('reporter.id', '==', reporterId)
                .where('status', '==', 'Active'))
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
              value: doc.payload.doc.data()['managementTime'],
            };
          });
        }),
      );
  }
}
