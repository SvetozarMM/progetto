import { Injectable } from '@angular/core';
import { ProjectDTO } from 'src/app/models/project.dto';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ProjectsService } from './projects.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectResolverService implements Resolve<ProjectDTO>{

  constructor(
    private readonly projectService: ProjectsService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<ProjectDTO> {
    const projectUrl = route.paramMap.get('projectUrl');

    return this.projectService.findProjectByUrl(projectUrl);
  }
}
