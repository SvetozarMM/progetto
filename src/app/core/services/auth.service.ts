import { Subject, BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { AuthData } from '../../models/auth-data.model';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { RegUser } from '../../models/user-register.dto';
import { AngularFirestore } from '@angular/fire/firestore';
import { switchMap } from 'rxjs/operators';
import { DocumentData } from '@google-cloud/firestore';
import { HttpClient } from '@angular/common/http';
import { RegEmployee } from '../../models/employee-register.dto';
import { NotificationService } from './notification.service';


@Injectable()
export class AuthService {
  isAuthenticated = new ReplaySubject<boolean>(1);
  userData: any;

  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone,
    public db: AngularFirestore,
    private http: HttpClient,
    private readonly notificator: NotificationService,
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        this.isAuthenticated.next(true);
      }
    });
  }

  async login(authData: AuthData) {
    return await this.afAuth.signInWithEmailAndPassword(authData.email, authData.password)
      .then((result) => {
        this.ngZone.run(() => {
          this.authSuccess();
        });
      }).catch((error) => {
        this.notificator.error(error);
      });
  }

  registerUser(user: RegUser): Observable<any> {
    return this.http
      .post<DocumentData>(
        'https://europe-west1-progetto-9bb63.cloudfunctions.net/register',
        {
          email: user.email,
          password: user.password,
        }
      ).pipe(
        switchMap((res) =>
          this.db.collection('users').doc(res.uid).set({ ...user, uid: res.uid })
        )
      );
  }
  registerEmployee(user: RegEmployee): Observable<any> {
    return this.http
      .post<DocumentData>(
        'https://europe-west1-progetto-9bb63.cloudfunctions.net/register',
        {
          email: user.email,
          password: user.password,
        }
      ).pipe(
        switchMap((res) =>
          this.db.collection('employees').doc(res.uid).set({ ...user, uid: res.uid })
        )
      );
  }


  async logout() {
    return await this.afAuth.signOut().then(() => {
      this.notificator.success('Logout successful');
      this.router.navigate(['/login']);
    });
  }

  private authSuccess() {
    this.router.navigate(['home/dashboard']);
  }
}
