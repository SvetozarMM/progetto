import { TestBed } from '@angular/core/testing';

import { ToggleCreateProjectBoxService } from './toggle-create-project-box.service';

describe('ToggleCreateProjectBoxService', () => {
  let service: ToggleCreateProjectBoxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ToggleCreateProjectBoxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
