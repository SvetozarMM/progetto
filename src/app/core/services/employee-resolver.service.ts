import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { EmployeeService } from './employee.service';
import { SingleEmployee } from '../../models/employee-single.dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeResolverService implements Resolve<SingleEmployee>{

  constructor(
    private readonly employeeService: EmployeeService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<SingleEmployee> {
    const id = route.paramMap.get('id');

    return this.employeeService.findEmployeeById(id);
  }
}
