import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SkillDTO } from 'src/app/models/skill.dto';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  constructor(
    private readonly db: AngularFirestore,
    private readonly notificator: NotificationService,
  ) { }

  getAllSkills(): Observable<SkillDTO[]> {
    return this.db
      .collection('skills')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray.map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
            };
          });
        }),
      );
  }

  addSkill(skillName: string): void {
    this.db
      .collection('skills')
      .add({name: skillName})
      .then(() => {
        this.notificator.success('Skill Added!');
      })
      .catch(() => this.notificator.error('Something went wrong!Try again.'));
  }
}
