import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsSectionComponent } from './components/projects-section/projects-section.component';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { CreateProjectBoxComponent } from './components/create-project-box/create-project-box.component';
import { CreateProjectBoxSkillBoxComponent } from './components/create-project-box-skill-box/create-project-box-skill-box.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SkillEmployeeBoxComponent } from './components/skill-employee-box/skill-employee-box.component';
import { SingleProjectComponent } from './components/single-project/single-project.component';
import { UpdateProjectBoxComponent } from './components/update-project-box/update-project-box.component';
import { UpdateProjectBoxSkillBoxComponent } from './components/update-project-box-skill-box/update-project-box-skill-box.component';

@NgModule({
  declarations: [
    ProjectsSectionComponent,
    CreateProjectBoxComponent,
    CreateProjectBoxSkillBoxComponent,
    SkillEmployeeBoxComponent,
    SingleProjectComponent,
    UpdateProjectBoxComponent,
    UpdateProjectBoxSkillBoxComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ProjectsSectionModule { }
