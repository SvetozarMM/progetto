import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ToggleCreateProjectBoxService } from '../../../core/services/toggle-create-project-box.service';
import { CreateProjectService } from '../../services/create-project.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ProjectsService } from 'src/app/core/services/projects.service';
import { ProjectTablePreviewDTO } from 'src/app/models/project-table-preview.dto';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { RegUser } from 'src/app/models/user-register.dto';
import { UserService } from 'src/app/core/services/user.service';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-projects-section',
  templateUrl: './projects-section.component.html',
  styleUrls: ['./projects-section.component.css']
})
export class ProjectsSectionComponent implements OnInit, AfterViewInit {

  isCreateProjectBoxOpen: boolean;
  allProjects: ProjectTablePreviewDTO[];
  //
  user: RegUser;
  //
  displayedColumns: string[] = [
    'name',
    'reporterFirstName',
    'reporterLastName',
    'status',
    'currentStatus',
    'start',
    'end',
    'target',
    'managementTime',
    'complete',
  ];
  dataSource: MatTableDataSource<ProjectTablePreviewDTO>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private readonly toggleCreateProjectBox: ToggleCreateProjectBoxService,
    private readonly createProjectService: CreateProjectService,
    private readonly projectService: ProjectsService,
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly notificator: NotificationService,
  ) { }

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => this.user = data);

    this.toggleCreateProjectBox.currentIsCreateProjectBoxOpen$
      .subscribe(value => this.isCreateProjectBoxOpen = value);
  }

  ngAfterViewInit() {
    this.projectService
      .getAllProjectsForTable()
      .subscribe(data => {
        this.allProjects = data;
        this.displayAllProjects();
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  displayAllProjects() {
    this.dataSource = new MatTableDataSource(this.allProjects);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  displayMyProjects() {
    this.dataSource = new MatTableDataSource(this.allProjects
      .filter(project => project.reporterId === this.user.uid));
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  showCreateBox() {
    if (this.user && this.user.freeTime > 0) {
      this.createProjectService.getAllSkills();
      this.createProjectService.getAllEmployees();
      this.toggleCreateProjectBox.show();
    } else {
      this.notificator
        .warning('You already work 8 hours a day. There is no time for management.');
    }
  }

  openProject(projectUrl: string) {
    this.router.navigate(['/home/projects', projectUrl]);
  }
}
