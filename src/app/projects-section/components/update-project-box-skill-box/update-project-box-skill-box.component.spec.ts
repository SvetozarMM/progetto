import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProjectBoxSkillBoxComponent } from './update-project-box-skill-box.component';

describe('UpdateProjectBoxSkillBoxComponent', () => {
  let component: UpdateProjectBoxSkillBoxComponent;
  let fixture: ComponentFixture<UpdateProjectBoxSkillBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProjectBoxSkillBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProjectBoxSkillBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
