import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProjectBoxSkillBoxComponent } from './create-project-box-skill-box.component';

describe('CreateProjectBoxSkillBoxComponent', () => {
  let component: CreateProjectBoxSkillBoxComponent;
  let fixture: ComponentFixture<CreateProjectBoxSkillBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProjectBoxSkillBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProjectBoxSkillBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
