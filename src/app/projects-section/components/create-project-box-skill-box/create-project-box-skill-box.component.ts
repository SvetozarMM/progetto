import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SkillDTO } from '../../../models/skill.dto';
import { CreateProjectService } from '../../services/create-project.service';
import { CreateProjectEmployeeDTO } from 'src/app/models/create-project-employee.dto';
import { CreateProjectCurrentEmployeeDTO } from 'src/app/models/create-project-current-employee.dto';
import { SkillBoxDTO } from 'src/app/models/skill-box.dto';
import { EmployeeProjectDTO } from 'src/app/models/employee-project.dto';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-create-project-box-skill-box',
  templateUrl: './create-project-box-skill-box.component.html',
  styleUrls: ['./create-project-box-skill-box.component.css']
})
export class CreateProjectBoxSkillBoxComponent implements OnInit {

  selectedSkill: SkillDTO;
  skillTarget = 0;
  skillComplete = '0';
  //
  sortedEmployeesList: CreateProjectEmployeeDTO[];
  employeesList: CreateProjectCurrentEmployeeDTO[] = [];
  //
  selectedEmployee: CreateProjectEmployeeDTO;
  freeHours = 0;
  workHours = 0;
  slectedHours: number;
  //
  currentSkillsList: SkillDTO[];

  @Output()
  eventAddSkillBox: EventEmitter<SkillBoxDTO> = new EventEmitter<SkillBoxDTO>();

  constructor(
    private readonly createProjectService: CreateProjectService,
    private readonly notificator: NotificationService,
  ) { }

  ngOnInit() {}

  getSkills() {
    this.refreshAddSkillBox();
    this.currentSkillsList = this.createProjectService.skillsList;
  }

  saveSelectedSkill(skill: SkillDTO) {
    if (skill) {
      this.selectedSkill = skill;
      this.sortedEmployeesList = this.createProjectService.currentEmployeesList
        .filter(employee => employee.skills.includes(skill.name) && employee.freeTime > 0);
    }
  }

  getEmployeeFreeTime(employeeFreeTime: number) {
    this.freeHours = employeeFreeTime;
  }

  getInfo() {
    if (!this.selectedSkill) {
      this.notificator
        .info('You must first select a skill to see employees who can work it.');
    }
  }

  saveTarget() {
    if (this.skillTarget < 0 || this.skillTarget.toString().match('[^0-9]')) {
      this.skillTarget = 0;
    }
    this.calculateCompleteDays();
  }

  saveWorkHours(selectedHours: number) {
    this.workHours = selectedHours;
  }

  addEmployee() {
    if (this.selectedEmployee) {
      this.employeesList.push({
        ...this.selectedEmployee,
        currentFreeTime: this.selectedEmployee.freeTime - this.workHours,
        currentWorkHours: this.workHours,
      });

      this.sortedEmployeesList = this.sortedEmployeesList
        .filter(employee => employee.id !== this.selectedEmployee.id);

      this.selectedEmployee = null;
      this.freeHours = 0;
      this.slectedHours = 0;
      this.workHours = 0;
    } else {
      this.notificator.warning('Nothing selected!');
    }
    this.calculateCompleteDays();
  }

  updateEmployee(currentEmployee: CreateProjectCurrentEmployeeDTO) {
    const index = this.employeesList.findIndex(employee => employee.id === currentEmployee.id);
    this.employeesList[index] = currentEmployee;
    this.calculateCompleteDays();
  }

  deleteEmployee(currentEmployee: CreateProjectCurrentEmployeeDTO) {
    this.employeesList = this.employeesList.filter(employee => employee.id !== currentEmployee.id);
    this.sortedEmployeesList.push(currentEmployee);
    this.calculateCompleteDays();
  }

  getCompleteBoxColor(): string {
    return this.skillComplete === 'never'
    ? 'var(--color-red-dark)'
    : this.skillComplete === '0' || this.skillComplete === 'pending'
    ? 'var(--color-yellow-dark)'
    : 'var(--color-green-dark)';
  }

  saveSkillBox() {
    if (this.selectedSkill) {
      this.eventAddSkillBox.emit({
            skill: this.selectedSkill,
            target: this.skillTarget,
            complete: this.skillComplete,
            employeesList: this.employeesList.map((item: EmployeeProjectDTO) => {
              item = {
                id: item.id,
                firstName: item.firstName,
                lastName: item.lastName,
                currentWorkHours: item.currentWorkHours
              };
              return item;
            }),
          });

      this.createProjectService.removeFromSkillsList(this.selectedSkill);
      this.createProjectService.updateEmployeesList(this.employeesList);
    } else {
      this.notificator.warning('You must select skill!');
    }
    this.refreshAddSkillBox();
  }

  private calculateCompleteDays() {
    const workHoursSum = this.employeesList.reduce((sum, employee) => {
      sum += employee.currentWorkHours;
      return sum;
    }, 0);

    this.skillComplete = workHoursSum === 0 && this.skillTarget > 0
    ? 'never'
    : workHoursSum === 0 && this.skillTarget === 0 || isNaN(this.skillTarget / workHoursSum)
    ? 'pending'
    : `${Math.ceil(this.skillTarget / workHoursSum)}`;
  }

  private refreshAddSkillBox() {
    this.selectedEmployee = null;
    this.skillTarget = 0;
    this.skillComplete = '0';
    this.employeesList = [];
    this.currentSkillsList = [];
    this.slectedHours = null;
    this.freeHours = 0;
  }
}
