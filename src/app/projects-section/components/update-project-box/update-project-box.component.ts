import { Component, OnInit, Input } from '@angular/core';
import { SkillBoxDTO } from 'src/app/models/skill-box.dto';
import { ProjectDTO } from 'src/app/models/project.dto';
import * as moment from 'moment';
import { StatusType } from 'src/app/common/enums/status-type';
import { CurrentStatusType } from 'src/app/common/enums/current-status-type';
import { UpdateProjectService } from '../../services/update-project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegUser } from 'src/app/models/user-register.dto';
import { UserService } from 'src/app/core/services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
const momentBusinessDays = require('moment-business-days');

@Component({
  selector: 'app-update-project-box',
  templateUrl: './update-project-box.component.html',
  styleUrls: ['./update-project-box.component.css']
})
export class UpdateProjectBoxComponent implements OnInit {

  user: RegUser;
  projectUpdateForm: FormGroup;
  //
  projectName: string;
  projectTarget = 0;
  projectComplete = '0';
  //
  skillBoxList: SkillBoxDTO[] = [];
  //
  reporterFreeHours = 0;
  managementTime: number;
  //
  startDate = new Date();
  endDate: Date;
  //
  project: ProjectDTO;

  constructor(
    private readonly updateProjectService: UpdateProjectService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.project = this.route.snapshot.data['project'];

    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
          if (this.user.freeTime > 0) {
            this.reporterFreeHours = this.user.freeTime + this.project.managementTime;
          } else {
            this.reporterFreeHours = this.project.managementTime;
          }
        }
      });

    if (this.project) {
      this.projectName = this.project.name;
      this.projectComplete = this.project.complete;
      this.endDate = moment(this.project.end, 'MM-DD-YYYY').toDate();
      this.skillBoxList = this.project.skillsBoxList;
      this.managementTime = this.project.managementTime;
    }

    this.projectUpdateForm = new FormGroup({
      projectDescription: new FormControl(this.project.description,
      {
        validators: [
          Validators.required,
          Validators.maxLength(300),
          Validators.minLength(10),
        ]
      }),
    });

    this.calculateNewTarget();
  }

  hideUpdateProjectBox() {
    this.router.navigate(['/home/projects', this.project.projectUrl]);
  }

  businessDayFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }

  saveTarget(selectedDate: Date): void {
    this.endDate = selectedDate;
    this.projectTarget = (momentBusinessDays(new Date()).businessDiff(momentBusinessDays(selectedDate))) + 1;
    this.calculateCompleteDays();
  }

  saveReporterWorkHours(selectedHours: number): void {
    this.managementTime = selectedHours;
  }

  addSkillBox(skillBox: SkillBoxDTO): void {
    this.skillBoxList.unshift(skillBox);
    this.calculateCompleteDays();
  }

  removeSkillBox(skillBox: SkillBoxDTO): void {
    this.updateProjectService.addToSkillsList(skillBox.skill);
    this.updateProjectService.returnEmployeesWorkHours(skillBox.employeesList);
    this.skillBoxList = this.skillBoxList
        .filter(data => data.skill.id !== skillBox.skill.id);
    this.calculateCompleteDays();
  }

  getCompleteBoxColor(complete: string): string {
    return complete === 'never' || +complete > this.projectTarget
    ? 'var(--color-red-dark)'
    : complete === '0' || complete === 'pending' || +complete === this.projectTarget
    ? 'var(--color-yellow-dark)'
    : 'var(--color-green-dark)';
  }

  updateProject() {
    this.updateProjectService.updateProject({
      id: this.project.id,
      name: this.projectName,
      description: this.projectUpdateForm.value.projectDescription,
      target: this.projectTarget,
      complete: this.projectComplete,
      end: moment(this.endDate).format('MM-DD-YYYY'),
      status: this.getProjectStatus(),
      currentStatus: this.getProjectCurrentStatus(),
      managementTime: this.managementTime,
      skillsBoxList: this.skillBoxList,
      projectUrl: this.projectName.replace(/[ ^]/g, '-').toLowerCase(),
    }, this.user, this.reporterFreeHours);
  }

  private calculateCompleteDays(): void {
    if (this.projectTarget === 0) {
      this.projectComplete = 'pending';
    } else if (this.projectTarget > 0 && !this.skillBoxList.length) {
      this.projectComplete = 'never';
    } else {
      this.projectComplete = this.skillBoxList.some(skillBox => skillBox.complete === 'never')
      ? 'never'
      : this.skillBoxList.reduce((max, skillBox) => {
        if (+skillBox.complete > +max ) {
          max = skillBox.complete;
        }
        return max;
      }, '0');
    }
  }

  private getProjectStatus(): string {
    return this.projectTarget > 0 || this.managementTime > 0 || this.skillBoxList.length
    ? StatusType.Active
    : StatusType.Planning;
  }

  private getProjectCurrentStatus(): string {
    return this.projectTarget >= +this.projectComplete
    || this.projectComplete !== 'never'
    ? CurrentStatusType.WithinTarget
    : this.projectTarget === 0
    ? CurrentStatusType.Pending
    : CurrentStatusType.OverdueTarget;
  }

  private calculateNewTarget(): void {
    const startDate = moment(this.project.start, 'MM-DD-YYYY').toDate();
    this.projectTarget = this.project.target - (momentBusinessDays(startDate)
        .businessDiff(momentBusinessDays(new Date())));
  }
}
