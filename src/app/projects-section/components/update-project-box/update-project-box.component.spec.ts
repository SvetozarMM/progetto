import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProjectBoxComponent } from './update-project-box.component';

describe('UpdateProjectBoxComponent', () => {
  let component: UpdateProjectBoxComponent;
  let fixture: ComponentFixture<UpdateProjectBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProjectBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProjectBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
