import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CreateProjectEmployeeDTO } from 'src/app/models/create-project-employee.dto';
import { CreateProjectCurrentEmployeeDTO } from 'src/app/models/create-project-current-employee.dto';

@Component({
  selector: 'app-skill-employee-box',
  templateUrl: './skill-employee-box.component.html',
  styleUrls: ['./skill-employee-box.component.css']
})
export class SkillEmployeeBoxComponent implements OnInit {

  @Input()
  employee: CreateProjectCurrentEmployeeDTO;
  workHours = 0;

  @Output()
  eventDeleteEmployee: EventEmitter<CreateProjectEmployeeDTO> = new EventEmitter<CreateProjectEmployeeDTO>();
  @Output()
  eventUpdateEmployee: EventEmitter<CreateProjectEmployeeDTO> = new EventEmitter<CreateProjectEmployeeDTO>();

  constructor( ) { }

  ngOnInit(): void {
  }

  saveWorkHours(selectedHours: number) {
    this.workHours = selectedHours;
  }

  updateEmployee(employee: CreateProjectCurrentEmployeeDTO) {
    const updatedEmployee = {
      ...employee,
      currentFreeTime: this.employee.freeTime - this.workHours,
      currentWorkHours: this.workHours,
    };
    this.eventUpdateEmployee.emit(updatedEmployee);
  }

  onDeleteEmployee(employee: CreateProjectCurrentEmployeeDTO) {
    this.eventDeleteEmployee.emit(employee);
  }
}
