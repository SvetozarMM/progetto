import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillEmployeeBoxComponent } from './skill-employee-box.component';

describe('SkillEmployeeBoxComponent', () => {
  let component: SkillEmployeeBoxComponent;
  let fixture: ComponentFixture<SkillEmployeeBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillEmployeeBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillEmployeeBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
