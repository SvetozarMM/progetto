import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProjectBoxComponent } from './create-project-box.component';

describe('CreateProjectBoxComponent', () => {
  let component: CreateProjectBoxComponent;
  let fixture: ComponentFixture<CreateProjectBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProjectBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProjectBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
