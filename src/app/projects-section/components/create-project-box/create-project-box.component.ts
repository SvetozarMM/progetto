import { Component, OnInit, Input } from '@angular/core';
import { ToggleCreateProjectBoxService } from 'src/app/core/services/toggle-create-project-box.service';
import { CreateProjectService } from '../../services/create-project.service';
import { SkillBoxDTO } from 'src/app/models/skill-box.dto';
import * as moment from 'moment';
import { StatusType } from 'src/app/common/enums/status-type';
import { CurrentStatusType } from 'src/app/common/enums/current-status-type';
import { RegUser } from 'src/app/models/user-register.dto';
import { UserService } from 'src/app/core/services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectTablePreviewDTO } from 'src/app/models/project-table-preview.dto';
import { NotificationService } from 'src/app/core/services/notification.service';
const momentBusinessDays = require('moment-business-days');


@Component({
  selector: 'app-create-project-box',
  templateUrl: './create-project-box.component.html',
  styleUrls: ['./create-project-box.component.css']
})
export class CreateProjectBoxComponent implements OnInit {

  @Input()
  allProjects: ProjectTablePreviewDTO[];
  user: RegUser;
  projectCreateForm: FormGroup;
  //
  projectTarget = 0;
  projectComplete = '0';
  //
  isCreateProjectBoxOpen: boolean;
  //
  skillBoxList: SkillBoxDTO[] = [];
  //
  reporterFreeHours = 0;
  managementTime = 0;
  //
  startDate = new Date();
  endDate: Date;

  constructor(
    private readonly toggleCreateProjectBox: ToggleCreateProjectBoxService,
    private readonly createProjectService: CreateProjectService,
    private readonly userService: UserService,
    private readonly notificator: NotificationService,
  ) { }

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
          this.reporterFreeHours = this.user.freeTime;
        }
      });
    this.toggleCreateProjectBox.currentIsCreateProjectBoxOpen$
      .subscribe(value => this.isCreateProjectBoxOpen = value);

    this.projectCreateForm = new FormGroup({
      projectName: new FormControl('', {
        validators: [
          Validators.required,
          Validators.maxLength(50),
          Validators.minLength(5),
        ]
      }),
      projectDescription: new FormControl('', {
        validators: [
          Validators.required,
          Validators.maxLength(300),
          Validators.minLength(10),
        ]
      }),
    });
  }

  hideCreateProjectBox(): void {
    this.toggleCreateProjectBox.hide();
  }

  businessDayFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  }

  saveTarget(selectedDate: Date): void {
    this.endDate = selectedDate;
    this.projectTarget = momentBusinessDays(new Date()).businessDiff(momentBusinessDays(selectedDate));
    this.calculateCompleteDays();
  }

  saveReporterWorkHours(selectedHours: number): void {
    this.managementTime = selectedHours;
  }

  addSkillBox(skillBox: SkillBoxDTO): void {
    this.skillBoxList.unshift(skillBox);
    this.calculateCompleteDays();
  }

  removeSkillBox(skillBox: SkillBoxDTO): void {
    this.skillBoxList = this.skillBoxList
        .filter(data => data.skill.id !== skillBox.skill.id);
    this.createProjectService.addToSkillsList(skillBox.skill);
    this.createProjectService.returnEmployeesWorkHours(skillBox.employeesList);
    this.calculateCompleteDays();
  }

  saveProject(): void {
    if (this.isProjectNameExist()) {
      this.createProjectService.createProject({
        name: this.projectCreateForm.value.projectName,
        description: this.projectCreateForm.value.projectDescription,
        target: this.projectTarget,
        complete: this.projectComplete,
        start: moment(this.startDate).format('MM-DD-YYYY'),
        end: moment(this.endDate).format('MM-DD-YYYY'),
        reporter: {
          id: this.user.uid,
          firstName: this.user.firstName,
          lastName: this.user.lastName,
        },
        status: this.getProjectStatus(),
        currentStatus: this.getProjectCurrentStatus(),
        managementTime: this.managementTime,
        skillsBoxList: this.skillBoxList,
        projectUrl: this.projectCreateForm.value.projectName.replace(/[ ^]/g, '-').toLowerCase(),
      }, this.user);
    }
  }

  getCompleteBoxColor(complete: string): string {
    return complete === 'never' || +complete > this.projectTarget
    ? 'var(--color-red-dark)'
    : complete === '0' || complete === 'pending' || +complete === this.projectTarget
    ? 'var(--color-yellow-dark)'
    : 'var(--color-green-dark)';
  }

  private calculateCompleteDays(): void {
    if (this.projectTarget === 0) {
      this.projectComplete = 'pending';
    } else if (this.projectTarget > 0 && !this.skillBoxList.length) {
      this.projectComplete = 'never';
    } else {
      this.projectComplete = this.skillBoxList.some(skillBox => skillBox.complete === 'never')
      ? 'never'
      : this.skillBoxList.reduce((max, skillBox) => {
        if (+skillBox.complete > +max ) {
          max = skillBox.complete;
        }
        return max;
      }, '0');
    }
  }

  private getProjectStatus(): string {
    return this.projectTarget > 0 || this.managementTime > 0 || this.skillBoxList.length
    ? StatusType.Active
    : StatusType.Planning;
  }

  private getProjectCurrentStatus(): string {
    return this.projectTarget >= +this.projectComplete
    || this.projectComplete !== 'never'
    ? CurrentStatusType.WithinTarget
    : this.projectTarget === 0
    ? CurrentStatusType.Pending
    : CurrentStatusType.OverdueTarget;
  }

  private isProjectNameExist(): boolean {
    const findProject = this.allProjects
      .find(proj => proj.name === this.projectCreateForm.value.projectName);

    if (findProject) {
      this.notificator.error('Project name already exist!');
    } else {
      return true;
    }
  }
}
