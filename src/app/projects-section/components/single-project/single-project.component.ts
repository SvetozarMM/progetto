import { Component, OnInit } from '@angular/core';
import { ProjectDTO } from 'src/app/models/project.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { UpdateProjectService } from '../../services/update-project.service';
import { SkillDTO } from 'src/app/models/skill.dto';
import { RegUser } from 'src/app/models/user-register.dto';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-single-project',
  templateUrl: './single-project.component.html',
  styleUrls: ['./single-project.component.css']
})
export class SingleProjectComponent implements OnInit {

  user: RegUser;
  project: ProjectDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly updateProjectService: UpdateProjectService,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.userService.user$
      .subscribe(data => {
        if (data) {
          this.user = data;
        }
      });

    this.project = this.route.snapshot.data['project'];
  }

  getCompleteBoxColor(complete: string): string {
    return complete === 'never' || +complete > this.project.target
    ? 'var(--color-red-dark)'
    : complete === '0' || complete === 'pending' || +complete === this.project.target
    ? 'var(--color-yellow-dark)'
    : 'var(--color-green-dark)';
  }

  returnToProjects() {
    this.router.navigate(['home/projects']);
  }

  openUpdateProjectBox() {
    const skillsToRemove: SkillDTO[] = this.project.skillsBoxList
      .reduce((skillsArr, skillBox) => {
        skillsArr.push(skillBox.skill);
        return skillsArr;
      }, []);

    this.router.navigate(['home/projects', this.project.projectUrl, 'update']);
    this.updateProjectService.getAllSkills(skillsToRemove);
  }

  changeProjectStatus() {
    this.updateProjectService.changeProjectStatusToEnded(this.project, this.user);
  }
}
