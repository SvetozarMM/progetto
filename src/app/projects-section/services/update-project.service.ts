import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { SkillDTO } from 'src/app/models/skill.dto';
import { CreateProjectEmployeeDTO } from 'src/app/models/create-project-employee.dto';
import { map, first } from 'rxjs/operators';
import { CreateProjectCurrentEmployeeDTO } from 'src/app/models/create-project-current-employee.dto';
import { EmployeeProjectDTO } from 'src/app/models/employee-project.dto';
import { UpdateProjectDTO } from 'src/app/models/update-project.dto';
import { RegUser } from 'src/app/models/user-register.dto';
import { StatusType } from 'src/app/common/enums/status-type';
import { ProjectDTO } from 'src/app/models/project.dto';
import { NotificationService } from 'src/app/core/services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class UpdateProjectService {

  skillsList: SkillDTO[];
  currentEmployeesList: CreateProjectEmployeeDTO[];

  constructor(
    private readonly db: AngularFirestore,
    private readonly router: Router,
    private readonly notificator: NotificationService,
  ) {
    this.db
      .collection('employees')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              freeTime: doc.payload.doc.data()['freeTime'],
              skills: doc.payload.doc.data()['skills'],
              currentProjects: doc.payload.doc.data()['currentProjects'],
              isModified: false,
            };
          });
        }),
        first()
      )
      .subscribe(data => {
        this.currentEmployeesList = data;
      });
  }

  getAllSkills(skillsToRemove: SkillDTO[]): void {
    this.db
      .collection('skills')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray.map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
            };
          });
        }),
        first()
      )
      .subscribe(data => this.skillsList = data
        .filter(skill => !skillsToRemove.some(item => item.id === skill.id)));
  }

  addToSkillsList(skill: SkillDTO): void {
    this.skillsList.push(skill);
  }

  removeFromSkillsList(skill: SkillDTO): void {
    this.skillsList = this.skillsList.filter(skills => skills.id !== skill.id);
  }

  updateEmployeesList(employeesToUpdate: CreateProjectCurrentEmployeeDTO[]): void {
    employeesToUpdate.forEach(employeeToUpdate => {
      this.currentEmployeesList.map(employee => {
          if (employee.id === employeeToUpdate.id) {
            employee.freeTime = employeeToUpdate.currentFreeTime;
          }
      });
    });
  }

  returnEmployeesWorkHours(employeesToUpdate: EmployeeProjectDTO[]): void {
    employeesToUpdate.forEach(employeeToUpdate => {
      this.currentEmployeesList.map(employee => {
          if (employee.id === employeeToUpdate.id) {
            employee.freeTime = employee.freeTime + employeeToUpdate.currentWorkHours;
          }
      });
    });
  }

  changeProjectStatusToEnded(project: ProjectDTO, user: RegUser) {
    const batch = this.db.firestore.batch();
    const projectRef = this.db.doc(`projects/${project.id}`).ref;
    const userRef = this.db.firestore.collection('users').doc(project.reporter.id);

    batch.update(projectRef, {
      status: StatusType.Ended,
      managementTime: 0,
      skillsBoxList: [],
    });

    batch.update(userRef, {
      freeTime: user.freeTime + project.managementTime,
    });

    const empArr = this.currentEmployeesList;

    project.skillsBoxList.forEach(item => {
      item.employeesList.forEach(emp => {
        empArr.forEach(employee => {
          if (employee.id === emp.id) {

            if (employee.currentProjects.includes(project.name)) {
              employee.currentProjects = employee.currentProjects
                .filter(name => name !== project.name);
            }

            employee.freeTime = employee.freeTime + emp.currentWorkHours;

            const employeeRef = this.db.firestore.collection('employees').doc(employee.id);

            batch.update(employeeRef, {
              ...employee,
            });
          }
        });
      });
    });

    batch.commit()
      .then(() => {
        this.router.navigate([`home/projects`]);
        this.notificator.success('Project is Ended!');
      })
      .catch(() => this.notificator.error('Something went wrong!Try again.'));

  }

  updateProject(
    project: UpdateProjectDTO,
    user: RegUser,
    reporterFreeHours: number,
    ) {
    const batch = this.db.firestore.batch();
    const projectRef = this.db.doc(`projects/${project.id}`).ref;
    const userRef = this.db.firestore.collection('users').doc(user.uid);

    batch.update(projectRef, {...project});

    batch.update(userRef, {
      freeTime: reporterFreeHours - project.managementTime,
    });

    const empArr: CreateProjectEmployeeDTO[] = this.currentEmployeesList;
    const currentWorkers = [];

    project.skillsBoxList.forEach(item => {
      item.employeesList.forEach(emp => {
        currentWorkers.push(emp.id);
      });
    });

    empArr.forEach(employee => {
      const employeeRef = this.db.firestore.collection('employees').doc(employee.id);

      if (currentWorkers.includes(employee.id)) {
        batch.update(employeeRef, {
          freeTime: employee.freeTime,
        });
      } else if (employee.currentProjects.includes(project.name)) {
          employee.currentProjects = employee.currentProjects
            .filter(name => name !== project.name);

          batch.update(employeeRef, {
            freeTime: employee.freeTime,
            currentProjects: employee.currentProjects,
          });
        }
    });

    batch.commit()
      .then(() => {
        this.router.navigate([`home/projects/${project.projectUrl}`]);
        this.notificator.success('Project Updated!');
      })
      .catch(() => this.notificator.error('Something went wrong!Try again.'));
  }
}
