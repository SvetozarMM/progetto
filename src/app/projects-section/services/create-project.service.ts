import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CreateProjectEmployeeDTO } from '../../models/create-project-employee.dto';
import { SkillDTO } from '../../models/skill.dto';
import { map, first } from 'rxjs/operators';
import { CreateProjectDTO } from '../../models/create-project.dto';
import { CreateProjectCurrentEmployeeDTO } from '../../models/create-project-current-employee.dto';
import { ToggleCreateProjectBoxService } from '../../core/services/toggle-create-project-box.service';
import { EmployeeProjectDTO } from '../../models/employee-project.dto';
import { RegUser } from 'src/app/models/user-register.dto';
import { NotificationService } from 'src/app/core/services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class CreateProjectService {

  skillsList: SkillDTO[];
  currentEmployeesList: CreateProjectEmployeeDTO[];

  constructor(
    private readonly db: AngularFirestore,
    private readonly toggleCreateProjectBox: ToggleCreateProjectBoxService,
    private readonly notificator: NotificationService,
  ) { }


  getAllSkills(): void {
    this.db
      .collection('skills')
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray.map(doc => {
            return {
              id: doc.payload.doc.id,
              name: doc.payload.doc.data()['name'],
            };
          });
        }),
        first()
      )
      .subscribe(data => this.skillsList = data);
  }

  getAllEmployees(): void {
    this.db
      .collection('employees', ref =>
      ref.where('freeTime', '>', 0)
      )
      .snapshotChanges()
      .pipe(
        map(docArray => {
          return docArray
          .map(doc => {
            return {
              id: doc.payload.doc.id,
              firstName: doc.payload.doc.data()['firstName'],
              lastName: doc.payload.doc.data()['lastName'],
              freeTime: doc.payload.doc.data()['freeTime'],
              skills: doc.payload.doc.data()['skills'],
              currentProjects: doc.payload.doc.data()['currentProjects'],
            };
          });
        }),
        first()
      )
      .subscribe(data => {
        this.currentEmployeesList = data;
      });
  }

  addToSkillsList(skill: SkillDTO): void {
    this.skillsList.push(skill);
  }

  removeFromSkillsList(skill: SkillDTO): void {
    this.skillsList = this.skillsList.filter(skills => skills.id !== skill.id);
  }

  updateEmployeesList(employeesToUpdate: CreateProjectCurrentEmployeeDTO[]): void {
    employeesToUpdate.forEach(employeeToUpdate => {
      this.currentEmployeesList.map(employee => {
          if (employee.id === employeeToUpdate.id) {
            employee.freeTime = employeeToUpdate.currentFreeTime;
          }
      });
    });
  }

  returnEmployeesWorkHours(employeesToUpdate: EmployeeProjectDTO[]): void {
    employeesToUpdate.forEach(employeeToUpdate => {
      this.currentEmployeesList.map(employee => {
          if (employee.id === employeeToUpdate.id) {
            employee.freeTime = employee.freeTime + employeeToUpdate.currentWorkHours;
          }
      });
    });
  }

  createProject(project: CreateProjectDTO, user: RegUser): void {
    const batch = this.db.firestore.batch();
    const projectRef = this.db.firestore.collection('projects').doc();
    const userRef = this.db.firestore.collection('users').doc(project.reporter.id);

    batch.set(projectRef, {...project});

    batch.update(userRef, {
      freeTime: user.freeTime - project.managementTime,
    });

    this.currentEmployeesList.forEach(employee => {
      project.skillsBoxList.forEach((skillBox) => {
        skillBox.employeesList.forEach((emp) => {
          if (emp.id === employee.id) {
            if (!employee.currentProjects.includes(project.name)) {
              employee.currentProjects.push(project.name);
            }
            const employeeRef = this.db.firestore.collection('employees').doc(employee.id);

            batch.update(employeeRef, {
              freeTime: employee.freeTime,
              currentProjects: employee.currentProjects,
            });
          }
        });
      });
    });

    batch.commit()
      .then(() => {
        this.toggleCreateProjectBox.hide();
        this.notificator.success('Project Created!');
      })
      .catch(() => this.notificator.error('Something went wrong!Try again.'));
  }
}
