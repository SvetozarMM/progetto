import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmploeeSectionComponent } from './components/emploee-section/emploee-section.component';
import { SingleEmployeeComponent } from './components/single-employee/single-employee.component';
import { SingleEmployeeProfileComponent } from './components/single-employee-profile/single-employee-profile.component';
import { SingleEmployeeProjectsComponent } from './components/single-employee-projects/single-employee-projects.component';
import { SingleUserComponent } from './components/single-user/single-user.component';
import { SingleUserProfileComponent } from './components/single-user/single-user-profile/single-user-profile.component';
import { SingleUserProjectsComponent } from './components/single-user/single-user-projects/single-user-projects.component';



@NgModule({
  declarations: [EmploeeSectionComponent, SingleEmployeeComponent, SingleEmployeeProfileComponent, SingleEmployeeProjectsComponent, SingleUserComponent, SingleUserProfileComponent, SingleUserProjectsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class EmploeeSectionModule { }
