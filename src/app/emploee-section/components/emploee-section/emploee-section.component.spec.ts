import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmploeeSectionComponent } from './emploee-section.component';

describe('EmploeeSectionComponent', () => {
  let component: EmploeeSectionComponent;
  let fixture: ComponentFixture<EmploeeSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmploeeSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmploeeSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
