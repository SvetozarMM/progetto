import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeTablePreviewDTO } from '../../../models/employee-table-preview.dto';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AuthService } from '../../../core/services/auth.service';
import { UserService } from '../../../core/services/user.service';
import { EmployeeService } from '../../../core/services/employee.service';
import { Router } from '@angular/router';
import { UserTablePreviewDTO } from '../../../models/user-table-preview.dto';
import { RegUser } from '../../../models/user-register.dto';

@Component({
  selector: 'app-emploee-section',
  templateUrl: './emploee-section.component.html',
  styleUrls: ['./emploee-section.component.css']
})
export class EmploeeSectionComponent implements OnInit {

  allEmployees: EmployeeTablePreviewDTO[];
  allManagers: UserTablePreviewDTO[];
  user: RegUser;

  displayedColumns: string[] = [
    'First Name',
    'Last Name',
    'Id',
    'Position',
    'Free Time',
  ];

  displayedColumns2: string[] = [
    'First Name',
    'Last Name',
    'Id',
    'Position',
    'Free Time',
  ];

  dataSource: MatTableDataSource<EmployeeTablePreviewDTO>;
  dataSource2: MatTableDataSource<UserTablePreviewDTO>;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly employeeService: EmployeeService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.userService.user$
    .subscribe(data => {
      if (data) {
        this.user = data;
      }
    });
    this.employeeService
      .getAllEmployeesForTable()
      .subscribe(data => {
        this.allEmployees = data;
        this.displayAllEmployees();
      });
    this.userService
      .getAllUsersForTable()
      .subscribe(data => {
        this.allManagers = data;
        this.displayAllManagers();
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  applyFilterManager(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  displayAllEmployees() {
    this.dataSource = new MatTableDataSource(this.allEmployees);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  displayAllManagers() {
    this.dataSource2 = new MatTableDataSource(this.allManagers);
    this.dataSource2.sort = this.sort;
    this.dataSource2.paginator = this.paginator;
  }

  openEmployeeCreate() {
    this.router.navigate(['/home/register-emp']);
  }

  openManagerCreate() {
    this.router.navigate(['/home/register']);
  }

  openEmployee(id: string) {
    this.router.navigate(['home/employees', id]);
  }

  openUser(uid: string) {
    this.router.navigate(['home/users', uid]);
  }

}
