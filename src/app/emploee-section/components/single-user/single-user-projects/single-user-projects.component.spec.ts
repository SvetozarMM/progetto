import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleUserProjectsComponent } from './single-user-projects.component';

describe('SingleUserProjectsComponent', () => {
  let component: SingleUserProjectsComponent;
  let fixture: ComponentFixture<SingleUserProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleUserProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleUserProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
