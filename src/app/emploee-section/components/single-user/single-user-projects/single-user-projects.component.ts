import { Component, OnInit } from '@angular/core';
import { SingleUser } from '../../../../models/user-single.dto';
import { UserTablePreviewDTO } from '../../../../models/user-table-preview.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectDashboardDTO } from '../../../../models/project-dashboard.dto';
import { UserService } from '../../../../core/services/user.service';

@Component({
  selector: 'app-single-user-projects',
  templateUrl: './single-user-projects.component.html',
  styleUrls: ['./single-user-projects.component.css']
})
export class SingleUserProjectsComponent implements OnInit {

  user: UserTablePreviewDTO;
  ownProjects: ProjectDashboardDTO[];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.user = this.route.snapshot.data['user'];
    this.userService.getUserOwnProjects(this.user.uid)
            .subscribe(projectData => this.ownProjects = projectData);
  }

}
