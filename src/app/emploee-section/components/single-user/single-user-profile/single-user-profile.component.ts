import { Component, OnInit } from '@angular/core';
import { UserTablePreviewDTO } from '../../../../models/user-table-preview.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../core/services/user.service';

@Component({
  selector: 'app-single-user-profile',
  templateUrl: './single-user-profile.component.html',
  styleUrls: ['./single-user-profile.component.css']
})
export class SingleUserProfileComponent implements OnInit {

  user: UserTablePreviewDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.user = this.route.snapshot.data['user'];
  }

}
