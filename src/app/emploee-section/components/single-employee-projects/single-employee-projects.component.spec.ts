import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleEmployeeProjectsComponent } from './single-employee-projects.component';

describe('SingleEmployeeProjectsComponent', () => {
  let component: SingleEmployeeProjectsComponent;
  let fixture: ComponentFixture<SingleEmployeeProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleEmployeeProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleEmployeeProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
