import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleEmployeeProfileComponent } from './single-employee-profile.component';

describe('SingleEmployeeProfileComponent', () => {
  let component: SingleEmployeeProfileComponent;
  let fixture: ComponentFixture<SingleEmployeeProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleEmployeeProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleEmployeeProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
