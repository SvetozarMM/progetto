import { Component, OnInit } from '@angular/core';
import { RegEmployee } from '../../../models/employee-register.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeTablePreviewDTO } from '../../../models/employee-table-preview.dto';

@Component({
  selector: 'app-single-employee-profile',
  templateUrl: './single-employee-profile.component.html',
  styleUrls: ['./single-employee-profile.component.css']
})
export class SingleEmployeeProfileComponent implements OnInit {

  employee: EmployeeTablePreviewDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.employee = this.route.snapshot.data['employee'];
    console.log(this.employee);
  }

  returnToEmployees() {
    this.router.navigate(['home/employees']);
  }

}
